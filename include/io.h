#ifndef __IO_H
#define __IO_H

#include "grille.h"
#include "jeu.h"
#include <stdio.h>

/**
 * \file io.h
 * \brief Déclaration des fonctions d'affichages mais aussi de la fonction
 * principale du programme : debut_jeu
 *
 * */
/**
 * \brief Affichage d'un trait horizontal
 */
void affiche_trait(int c);

/**
 * \brief Affichage d'une ligne de la grille
 */
void affiche_ligne(int c, int *ligne);

/**
 * \brief Affichage d'une grille
 */
void affiche_grille(grille g);

/**
 * \brief Effacement d'une grille
 */
void efface_grille(grille g);

/**
 * \brief Débute le jeu
 */
void debut_jeu(grille *g, grille *gc);

/**
 * \brief Affiche l'évolution par pas de temps
 */
void affiche_evolution(int i);

#endif
