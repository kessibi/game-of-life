#ifndef __JEU_H
#define __JEU_H

#include "grille.h"
/**
 * \file jeu.h
 * \brief Déclaration de fonctions nécessaires au gameplay du Jeu de la Vie
 *
 * */
/**
 * \brief modulo modifié pour traiter correctement les bords i=0 et j=0
 * dans le calcul des voisins avec bords cycliques
 * \param i le divisé
 * \param m le diviseur
 * \return le modulo de i par m
 */
static inline int modulo(int i, int m) { return (i + m) % m; }

/**
 * compte le nombre de voisins vivants de la cellule (i,j)
 * les bords sont cycliques (si la cellule est en bord de grille il cherchera
 * les cellules "voisines" correspondantes grâce au modulo \param i et j entiers
 * pour ligne et colonne, g la grille inspectée \return Le nombre de voisins
 * cycliques vivants
 */
int compte_voisins_vivants(int i, int j, grille g);

/**
 * \brief Fait évoluer la grille g d'un pas de temps
 */
void evolue(grille *g, grille *gc,
            int (*compte_non_cyclique)(int, int, grille));

/**
 * \brief Fait évoluer la grille g d'un pas de temps en prenant compte du
 * vieillissement des cellules
 */
void evolution_vieillissement(grille *g, grille *gc,
                              int (*compte_non_cyclique)(int, int, grille));

/**
 * compte le nombre de voisins vivants de la cellule (i,j)
 * les bords ne sont pas cycliques, si une cellule est en bord de cellule elle
 * aura moins de voisines que celles au centre \param i et j entiers pour ligne
 * et colonne, g la grille inspectée \return Le nombre de voisins vivants
 */
int compte_non_cyclique(int i, int j, grille g);
#endif
