#ifndef __GRILLE_H
#define __GRILLE_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * \file grille.h
 * \brief Déclaration des fonctions d'allocation, de désallocation de grilles et
 * de gestion de cellules (morte, vivante..)
 *
 * */

/**
 * \struct grille grille.h
 * \brief Définit une une grille
 */
typedef struct {
  int nbl;        /**< Nombre de lignes*/
  int nbc;        /**< Nombre de colonnes*/
  int **cellules; /**< Tableau de tableau de cellules*/
} grille;

/**
 * \brief Allocation d'une grille de taille l*c
 * \brief Initialisation de toutes les cellules à mortes
 */
void alloue_grille(int l, int c, grille *g);

/**
 * \brief Desallocation d'une grille
 */
void libere_grille(grille *g);

/**
 * \brief Alloue et initalise la grille g à partir d'un fichier
 * \param filename Fichier .txt dessinant une grille
 * \param g La grille initialisée
 */
void init_grille_from_file(char *filename, grille *g);

/**
 * \brief Donne vie à la cellule (i,j) de la grille g
 * \param g La grille ou l'on définie une des cellules comme vivante
 * \param i selecteur sur ligne
 * \param j selecteur sur colonne
 */
static inline void set_vivante(int i, int j, grille g) { g.cellules[i][j] = 1; }

/**
 * \brief Définit la cellule comme non viable (i,j) de la grille g
 * \param g La grille ou l'on définie une des cellules comme vivante
 * \param i selecteur sur ligne
 * \param j selecteur sur colonne
 */
static inline void set_nonviable(int i, int j, grille g) {
  g.cellules[i][j] = -1;
}

static inline int est_nonviable(int i, int j, grille g) {
  return g.cellules[i][j] == -1;
}
/**
 * \brief Donne la mort à la cellule (i,j) de la grille g
 * \param g La grille ou l'on définie une des cellules comme vivante
 * \param i selecteur sur ligne
 * \param j selecteur sur colonne
 */
static inline void set_morte(int i, int j, grille g) { g.cellules[i][j] = -2; }

/**
 * \brief Teste si la cellule (i,j) de la grille g est vivante
 */
static inline int est_vivante(int i, int j, grille g) {
  if (g.cellules[i][j] == -2)
    return 0;
  else if (g.cellules[i][j] == -1)
    return 0;
  else
    return 1;
}
/**
 * \brief Recopie gs dans gd (sans allocation)
 */
void copie_grille(grille gs, grille gd);

#endif
