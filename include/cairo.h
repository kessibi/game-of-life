#ifndef __CAIRO
#define __CAIRO
#include "../include/grille.h"
#include "../include/jeu.h"
#include <X11/Xlib.h>
#include <cairo-xlib.h>
#include <cairo.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * \file cairo.h
 * \brief Déclaration et définition des fonctions paintLigne et paintGrille
 * utilisant la librairie cairo
 *
 * */

/**
 * \brief Paint une ligne de grille à partir d'une ligne spécifique et des
 * éléments cairo nécessaires
 */
void paintLigne(int a, int c, int *ligne, cairo_surface_t *surface,
                cairo_t *cr) {
  int i;
  for (i = 0; i < c; i++) {
    if (ligne[i] == -1) {
      cairo_rectangle(cr, 20 + (20 * i + 50 * i), 20 + (20 * a + 50 * a), 50,
                      50);
      cairo_set_source_rgb(cr, 1.0, 0.0, 1.0);
      cairo_fill(cr);
    } else if (ligne[i] == -2) {
      cairo_rectangle(cr, 20 + (20 * i + 50 * i), 20 + (20 * a + 50 * a), 50,
                      50);
      cairo_set_source_rgb(cr, 1.0, 1.0, 1.0);
      cairo_fill(cr);
    } else {
      cairo_rectangle(cr, 20 + (20 * i + 50 * i), 20 + (20 * a + 50 * a), 50,
                      50);
      cairo_set_source_rgb(cr, 0.0, 0.1 + ligne[i] * 0.1, 0.0);
      cairo_fill(cr);
    }
  }
}

/**
 * \brief Paint une grille sur la fenêtre cairo en utilisant la bibliothèque
 * cairo
 */
void paintGrille(grille g, cairo_surface_t *surface) {
  cairo_t *cr;
  cr = cairo_create(surface);
  cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
  cairo_paint(cr);
  // dessin grille
  int i, l = g.nbl, c = g.nbc;
  for (i = 0; i < l; i++) {
    paintLigne(i, c, g.cellules[i], surface, cr);
  }
}

#endif
