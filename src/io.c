#include "../include/io.h"
#include "../include/grille.h"

void affiche_trait(int c) {
  int i;
  for (i = 0; i < c; ++i)
    printf("|---");
  printf("|\n");
  return;
}

void affiche_ligne(int c, int *ligne) {
  int i;
  for (i = 0; i < c; ++i)
    if (ligne[i] == -2)
      printf("|   ");
    else if (ligne[i] == -1)
      printf("| X ");
    else
      printf("| %d ", ligne[i]);
  printf("|\n");
  return;
}

void affiche_grille(grille g) {
  int i, l = g.nbl, c = g.nbc;
  printf("\n");
  affiche_trait(c);
  for (i = 0; i < l; ++i) {
    affiche_ligne(c, g.cellules[i]);
    affiche_trait(c);
  }
  printf("\n");
  return;
}

void efface_grille(grille g) { printf("\n\e[%dA", g.nbl * 2 + 7); }

void affiche_evolution(int i) { printf("Nombre d'évolutions: %d\n", i); }

void debut_jeu(grille *g, grille *gc) {
  char c = getchar();
  int i = 0;
  int (*compte)(int, int, grille);
  compte = compte_voisins_vivants;
  void (*evo)(grille *, grille *, int (*)(int, int, grille));
  evo = evolue;

  while (c != 'q') // touche 'q' pour quitter
  {
    // si le caractère lu est 'n', lire la string et tout réallouer et
    // initialiser la grille
    switch (c) {
    case '\n': { // touche "entree" pour évoluer
      evo(g, gc, compte);
      printf("\n");
      efface_grille(*g);
      i++;
      affiche_evolution(i);
      affiche_grille(*g);
      break;
    }
    // touhe 'n' pour changer de grille à jouer
    case 'n': {
      i = -1;
      char string[256];
      printf("Entrez un chemin vers une grille.txt\n");
      scanf("%s", string);
      printf("%s", string);
      init_grille_from_file(string, g);
      alloue_grille(g->nbl, g->nbc, gc);
      affiche_grille(*g);
      printf("\n");
      break;
    }
    // touche 'c' pour changer le comptage des voisins vivants
    case 'c': {
      if (compte == compte_voisins_vivants) {
        compte = compte_non_cyclique;
        printf("\33[2K");
        printf("Compte de voisins vivants : Non cyclique");
      } else {
        compte = compte_voisins_vivants;
        printf("\33[2K");
        printf("Compte de voisins vivants : Cyclique");
      }
      break;
    }
    // touche 'v' pour activer ou non le vieillissement des cellules
    case 'v': {
      if (evo == evolue) {
        evo = evolution_vieillissement;
        printf("\33[2K");
        printf("Vieillissement : Actif");
      } else {
        evo = evolue;
        printf("\33[2K");
        printf("Vieillissement : Passif");
      }
      break;
    }
    default: { // touche non traitée
      printf("\n\e[1A");
      break;
    }
    }
    c = getchar();
  }
  return;
}
