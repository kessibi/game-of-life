#include "../include/jeu.h"

int compte_voisins_vivants(int i, int j, grille g) {
  int v = 0, l = g.nbl, c = g.nbc;
  v += est_vivante(modulo(i - 1, l), modulo(j - 1, c), g);
  v += est_vivante(modulo(i - 1, l), modulo(j, c), g);
  v += est_vivante(modulo(i - 1, l), modulo(j + 1, c), g);
  v += est_vivante(modulo(i, l), modulo(j - 1, c), g);
  v += est_vivante(modulo(i, l), modulo(j + 1, c), g);
  v += est_vivante(modulo(i + 1, l), modulo(j - 1, c), g);
  v += est_vivante(modulo(i + 1, l), modulo(j, c), g);
  v += est_vivante(modulo(i + 1, l), modulo(j + 1, c), g);

  return v;
}

int compte_non_cyclique(int i, int j, grille g) {
  int l = g.nbl, c = g.nbc;
  int v = 0;
  if (i - 1 >= 0 && j - 1 >= 0 && est_vivante(i - 1, j - 1, g))
    v++;
  if (i - 1 >= 0 && est_vivante(i - 1, j, g))
    v++;
  if (i - 1 >= 0 && j + 1 < c && est_vivante(i - 1, j + 1, g))
    v++;
  if (j - 1 >= 0 && est_vivante(i, j - 1, g))
    v++;
  if (j + 1 < c && est_vivante(i, j + 1, g))
    v++;
  if (i + 1 < l && j - 1 >= 0 && est_vivante(i + 1, j - 1, g))
    v++;
  if (i + 1 < l && est_vivante(i + 1, j, g))
    v++;
  if (i + 1 < l && j + 1 < c && est_vivante(i + 1, j + 1, g))
    v++;
  return v;
}

void evolue(grille *g, grille *gc, int (*compte)(int, int, grille)) {
  copie_grille(*g, *gc); // copie temporaire de la grille
  int i, j, l = g->nbl, c = g->nbc, v;
  for (i = 0; i < l; i++) {
    for (j = 0; j < c; ++j) {
      v = compte(i, j, *gc);
      if (est_nonviable(i, j, *g)) {
        set_nonviable(i, j, *g);
      } else if (est_vivante(i, j, *g)) { // evolution d'une cellule vivante
        if (v != 2 && v != 3)
          set_morte(i, j, *g);
      } else { // evolution d'une cellule morte
        if (v == 3)
          set_vivante(i, j, *g);
      }
    }
  }
  return;
}

void evolution_vieillissement(grille *g, grille *gc,
                              int (*compte)(int, int, grille)) {
  copie_grille(*g, *gc); // copie temporaire de la grille
  int i, j, l = g->nbl, c = g->nbc, v;
  for (i = 0; i < l; i++) {
    for (j = 0; j < c; ++j) {
      v = compte(i, j, *gc);
      if (est_vivante(i, j, *g)) { // evolution d'une cellule vivante
        if (v != 2 && v != 3)
          set_morte(i, j, *g);
        else if (g->cellules[i][j] == 8) {
          set_morte(i, j, *g);
        } else {
          g->cellules[i][j]++;
        }
      } else if (est_nonviable(i, j, *g)) {
        set_nonviable(i, j, *g);
      } else { // evolution d'une cellule morte
        if (v == 3)
          set_vivante(i, j, *g);
      }
    }
  }
  return;
}
