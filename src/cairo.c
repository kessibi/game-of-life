#include "../include/cairo.h"

#define SIZEX 600
#define SIZEY 600

int main (int argc, char *argv[]){
	// X11 display
	Display *dpy;
	Window rootwin;
	Window win;
	XEvent e;
	int scr;

  if(argc != 2) {
    fprintf(stderr, "usage: %s <grille>\n", argv[0]);
    exit(1);
  }

	// init the display
	if(!(dpy=XOpenDisplay(NULL))) {
		fprintf(stderr, "ERROR: Could not open display\n");
		exit(1);
	}

	scr=DefaultScreen(dpy);
	rootwin=RootWindow(dpy, scr);

	win=XCreateSimpleWindow(dpy, rootwin, 1, 1, SIZEX, SIZEY, 0, 
			BlackPixel(dpy, scr), BlackPixel(dpy, scr));	
			
			

	XStoreName(dpy, win, "Jeu de la Vie");
	XSelectInput(dpy, win, ExposureMask|ButtonPressMask|KeyPressMask|KeyReleaseMask);
	XMapWindow(dpy, win);
	
	// create cairo surface
	cairo_surface_t *cs; 
	cs=cairo_xlib_surface_create(dpy, win, DefaultVisual(dpy, 0), SIZEX, SIZEY);

	//grille + allocation de cette derniere
	int boucle = 1;
	int i = 1;
	grille g, gc;
	init_grille_from_file(argv[1],&g);
	alloue_grille (g.nbl, g.nbc, &gc);
	printf("Vieillissement : NON\n");
	printf("Compte de voisins vivants : Est Cyclique\n");
	
	//pointeurs sur fonctions
	int (*compte) (int, int, grille);
	compte = compte_voisins_vivants;
	void (*evo) (grille*, grille*, int(*)(int,int, grille));
	evo = evolue;
	// run the event loop
	while(boucle) {
		XNextEvent(dpy, &e);
		paintGrille(g, cs);
		printf("\n\e[0A");
		if(e.type==ButtonPress){
			switch(e.xbutton.button){
				case Button1:
					i++;
					printf("\n\e[1A");
					printf("\33[2K");
					printf("\rNombre d'évolutions: %d", i);
					evo(&g,&gc, compte);
					paintGrille(g, cs);
					break;
				case Button3:
					boucle = 0;
					libere_grille(&g);
					libere_grille(&gc);
					break;
			}
		} else if(e.type==KeyPress){
			int x = e.xkey.keycode;
			switch(x){
				case 54:
					if(compte == compte_voisins_vivants){
						compte = compte_non_cyclique;
						printf("\33[3K");
						printf("\n\e[2A");
						printf("Compte de voisins vivants : Non Cyclique\n");
					}
					else {
						compte = compte_voisins_vivants;
						printf("\33[3K");
						printf("\n\e[2A");
						printf("Compte de voisins vivants : Est Cyclique\n"); 
					}
				break;
				case 55:
					if(evo == evolue){
						evo = evolution_vieillissement;
						printf("\33[4K");
						printf("\n\e[3A");
						printf("Vieillissement : OUI\n\n");
					} else{
						evo = evolue;
						printf("\33[4K");
						printf("\n\e[3A");
						printf("Vieillissement : NON\n\n");
					}
				break;
				case 57:
					i=-1;
					char string[256];
					printf("\rEntrez un chemin vers une grille.txt\n");
					scanf("%s", string);
					init_grille_from_file(string, &g);
					alloue_grille (g.nbl, g.nbc, &gc);
				break;
			}
			
		}
	}
	cairo_surface_destroy(cs);
	XCloseDisplay(dpy);
	return 0;
}

