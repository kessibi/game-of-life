# Game of Life

## Context

This game was made during my 3rd semester at University. It was originally
designed for the module 'Development Techniques'. This module teached us about
Git, C, Makefile, and some basic development skills.

This project was originally hosted on the University of Strasbourg's git server.
I have decided to move it here.

## How to run

Compile the game with `make text=0` for a cli version of the game or
`make text=1` for a windowed and colered one. A documentation is available after
 a quick `make doxygen` in `doc/html`.

Run the game with `./bin/main <grid file>`. There is an example grid file at the
root of the project.

## How to play

On the cli version you make life go on by pressing enter, on the windowed
version, it is by left clicking.
