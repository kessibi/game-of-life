APP = main
CC = gcc
FLAGS = -std=c99 -Wall -Werror
IFLAGS = -Iinclude -I/usr/include/cairo
LFLAGS = -lcairo -lm -lX11
SRC_REP = src
INCLUDE_REP = include
O_REP = obj
BIN_REP = bin
DOX_REP = doc
text = 0

SOURCES  := $(wildcard $(SRC_REP)/*.c)
INCLUDES := $(wildcard $(INCLUDE_REP)/*.h)
OBJECTS  := $(SOURCES:$(SRC_REP)/%.c=$(O_REP)/%.o)
DOXYFILE := $(DOX_REP)/Doxyfile

$(BIN_REP)/$(APP): $(OBJECTS)
ifeq ($(text), 0)
	$(CC) $(filter-out obj/cairo.o,$(OBJECTS)) -o $@
else
	$(CC) $(filter-out obj/main.o,$(OBJECTS)) -o $@ $(IFLAGS) $(LFLAGS)
endif


$(OBJECTS): $(O_REP)/%.o : $(SRC_REP)/%.c dirs
ifeq ($(text), 0)
	@if [ ! $< = "src/cairo.c" ]; then\
		echo $(CC) $(FLAGS) -c $< -o $@;\
		$(CC) $(FLAGS) -c $< -o $@;\
	fi
else
	$(CC) $(FLAGS) -c $< -o $@ $(IFLAGS) $(LFLAGS)
endif

dirs:
	rm -rf $(O_REP)
	rm -rf $(BIN_REP)
	mkdir $(O_REP)
	mkdir $(BIN_REP)

clean:
	rm -rf $(O_REP)
	rm -rf $(BIN_REP)
	rm -rf doc/html

doxygen:
	doxygen doc/Doxyfile


archive : $(SOURCES) $(INCLUDES)
	tar -zcvf game-of-life.tar.gz $(SOURCES) $(INCLUDES) Makefile $(DOXYFILE)

	
